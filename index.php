<?php

use Media\Capitulo;
use Media\Serie;
use Media\Temporada;

require 'vendor/autoload.php';

$capitulo = new Capitulo(45, 'Piloto', '2021/01/26', 4.3, 1);
$capitulo2 = new Capitulo(45, 'Finale', '2021/03/02', 2.8, 8);
$capitulo3 = new Capitulo(46, 'Echoes', '2022/01/18', 3.6, 1);
$capitulo4 = new Capitulo(44, 'Revelations', '2022/01/25', 1.2, 2);

$temporada = new Temporada(1, 2021);
$temporada->addEpisode($capitulo);
$temporada->addEpisode($capitulo2);

$temporada2 = new Temporada(2, 2022);
$temporada2->addEpisode($capitulo3);
$temporada2->addEpisode($capitulo4);

$serie = new Serie('Upgrade-Hub', 'Terror');
$serie->addSeason($temporada);
$serie->addSeason($temporada2);

echo "La temporada 1 tiene " . $serie->getSeasonEpisodes(1) . " episodio/s.<br>";
echo "La temporada 2 tiene " . $serie->getSeasonEpisodes(2) . " episodio/s.<br>";

echo "La serie tiene una valoración media de " . $serie->getAverageRating() . ".";
