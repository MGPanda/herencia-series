<?php

namespace Media;

use Media\Base\ElementoMultimedia;

class Capitulo extends ElementoMultimedia
{
    private $title;
    private $premiereDate;
    private $rating;
    private $epNumber;

    function __construct($length, $title, $premiereDate, $rating, $epNumber)
    {
        parent::__construct($length);
        $this->title = $title;
        $this->premiereDate = $premiereDate;
        $this->rating = $rating;
        $this->epNumber = $epNumber;
    }

    function getRating()
    {
        return $this->rating;
    }
}
