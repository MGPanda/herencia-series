<?php

namespace Media;

class Serie
{
    private $name;
    private $genre;
    private $seasons = [];

    function __construct($name, $genre)
    {
        $this->name = $name;
        $this->genre = $genre;
    }

    function addSeason($season)
    {
        array_push($this->seasons, $season);
    }

    function getSeasonEpisodes($seasonNum)
    {
        $index = $seasonNum - 1;
        if ($this->seasons[$index]) {
            return $this->seasons[$index]->getEpisodeCount();
        } else {
            echo "La temporada $seasonNum no existe.";
        }
    }

    function getAverageRating()
    {
        $acc = 0;

        foreach ($this->seasons as $season) {
            $acc += $season->getAverageRating();
        }

        return $acc / count($this->seasons);
    }
}
