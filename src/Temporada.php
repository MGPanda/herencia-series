<?php

namespace Media;

class Temporada
{
    private $seasonNum;
    private $year;
    private $episodes = [];

    function __construct($seasonNum, $year)
    {
        $this->seasonNum = $seasonNum;
        $this->year = $year;
    }

    function addEpisode($episode)
    {
        array_push($this->episodes, $episode);
    }

    function getEpisodeCount()
    {
        return count($this->episodes);
    }

    function getAverageRating()
    {
        $acc = 0;

        foreach ($this->episodes as $episode) {
            $acc += $episode->getRating();
        }

        return $acc / count($this->episodes);
    }
}
